Задания решены исходя из соображения оптимизации обращения к данным: извлечение данных из RDD
только после того, как выполнены все трансформации, нужные для решения задачи.

Зависимости в мавен Pom.xml.

**Три файла, три функции main, три отдельных подпрограммы.**

**Scala 2.10.6**

## 3 задания с текстовым процессингом
Код в ```src/SpartTextProcessor```.

Работают с двумя файлами в ```assets```.

Аргументы: ```local[*] assets/text-example-1.md assets/text-example-2.md```

## 3 задания с процессингом матриц
Код в ```SparkMatrixProcessor```;

Аргументы: ```local[*]```;


## 3 задания с процессингом матриц при помощи spark.mllib
Код в ```MllibMatrixProcessor```;

Аргументы: ```local[*]```;
