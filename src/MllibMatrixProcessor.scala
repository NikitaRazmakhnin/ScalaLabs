import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.linalg.distributed.{CoordinateMatrix, MatrixEntry}
import org.apache.spark.rdd.RDD

/*
  4. Дана матрица чисел. Проверить, является ли матрица симметричной относительно главной диагонали.
  5. Написать программу перемножения двух матриц.
  6. Дана матрица a. Сформировать новую матрицу где каждый элемент aij заменен на значение выражения
  aij = ai+2,j + ai-2,j + ai,j+2 + ai,j-2 + 4ai,j
* */

object MllibMatrixProcessor {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("SparkTextProcessor")
      .setMaster(args(0))

    val context = new SparkContext(conf)

    Logger
      .getRootLogger
      .setLevel(Level.ERROR)

    val matrixA = Array(
      Array(4, 4, 1, 4, 4),
      Array(4, 4, 5, 3, 4),
      Array(1, 5, 2, 1, 3),
      Array(4, 3, 1, 2, 3),
      Array(4, 4, 3, 3, 5)
//      Array(7, 4, 3, 3, 5)
    )

    val matrixB = Array(
      Array(4, 4, 1, 4, 4, 6),
      Array(4, 4, 5, 3, 4, 7),
      Array(1, 5, 2, 1, 3, 1),
      Array(4, 3, 1, 2, 3, 3),
      Array(4, 4, 3, 3, 5, 9)
      // Array(7, 4, 3, 3, 5, 9)
    )

    // first tast
    val isSymmetric = this.isSymmetric(context.parallelize(matrixA))
    println("Matrix is %s".format(if (isSymmetric) "symmetric" else "unsymmetric"))

    // second task
    val multiplied = this.multiply(context.parallelize(matrixA), context.parallelize(matrixB)).collect()
    println("Multiplied matrix:")
    SparkMatrixProcessor.showMatrix(multiplied)

    // third task
    val calculated = this.calcByFormula(context.parallelize(matrixB)).collect()
    println("\nCalculated matrix:")
    SparkMatrixProcessor.showMatrix(calculated)
  }

  def isSymmetric(matrix : RDD[Array[Int]]) : Boolean = {
    val coordinated = getCoordinatedMatrix(matrix)
    coordinated.transpose().entries.subtract(coordinated.entries).isEmpty()
  }

  def getCoordinatedMatrix(matrix : RDD[Array[Int]]) : CoordinateMatrix = {
    new CoordinateMatrix(
      SparkMatrixProcessor
        .getFlattenIndexedMatrix(matrix)
        .map(entry => MatrixEntry(entry._1.toLong, entry._2.toLong, entry._3.toDouble))
    )
  }

  def multiply(matrixA : RDD[Array[Int]], matrixB : RDD[Array[Int]]) : RDD[Array[Int]] = {
    val coordinatedA = this.getCoordinatedMatrix(matrixA)
    val coordinatedB = this.getCoordinatedMatrix(matrixB)

    coordinatedA
      .toBlockMatrix()
      .multiply(coordinatedB.toBlockMatrix())
      .toCoordinateMatrix()
      .entries
      // then convert to simple matrix
      .groupBy(entry => entry.i)
      .map(row => (row._1, row._2.map(cell => cell.value.toInt).toArray))
      .sortBy(row => row._1)
      .map(row => row._2)
  }

  def calcByFormula(matrix : RDD[Array[Int]]) : RDD[Array[Int]] = {
    /** Calc by formula: aij = ai+2,j + ai-2,j + ai,j+2 + ai,j-2 + 4ai,j */
    val coordinated = this.getCoordinatedMatrix(matrix)
    val all = coordinated.entries.cartesian(coordinated.entries)

    // i have no idea about solution with mllib here, so got
    // almost same solution, as without mllib, but coordinate matrix entries more convenient
    val fourthComponent = all.filter(pair => pair._1.i == pair._2.i && pair._1.j - 2 == pair._2.j)  // get pairs with ai,j-2 and ai,j
    val thirdComponent = all.filter(pair => pair._1.i == pair._2.i && pair._1.j + 2 == pair._2.j)   // get pairs with ai,j+2 and ai,j
    val secondComponent = all.filter(pair => pair._1.i - 2 == pair._2.i && pair._1.j == pair._2.j)  // get pairs with ai-2,j and ai,j
    val firstComponent = all.filter(pair => pair._1.i + 2 == pair._2.i && pair._1.j == pair._2.j)   // get pairs with ai+2,j and ai,j

    Array(firstComponent, secondComponent, thirdComponent, fourthComponent)
      .map(_.map(pair => (pair._2.i.toString + pair._2.j.toString, pair._1, pair._2))) // mark by indexes id this pair
      .reduce(_.union(_))       // unite all pairs
      .groupBy(pair => pair._1) // group all components by new cell indexes
      .map(cell => {
        // calculate according formula (second component and third number)
        val parts = cell._2.toArray
        val value = parts.map(part => part._2.value).sum + parts(1)._3.value * 4
        (cell._1, value)
      })
      // create new matrix
      .map(cell => {
        val row = cell._1(0)
        val col = cell._1(1)
        (row, col, cell._2)
      })
      .groupBy(cell => cell._1)
      .map(row => row._2.toArray.map(cell => cell._3.toInt))
  }
}
