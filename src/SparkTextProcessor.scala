import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}


/*
1. На основе заданного текста в файле составить словарь — то есть список
всех уникальных слов, используемых в тексте в алфавитном порядке.
Знаки препинания игнорировать.

2. На основе заданного текста подсчитать вхождение каждого слова. То
есть словарь в котором для каждого слова указывается сколько раз оно
входит в текст.

3. Даны два разных текста. Сформировать 3 множества: пересечение
словарей заданных текстов, множество слов, входящих только в первый
текст, множество слов, входящих только во второй текст.

*/

object SparkTextProcessor {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SparkTextProcessor").setMaster(args(0))
    val context = new SparkContext(conf)
    Logger.getRootLogger.setLevel(Level.ERROR)

    val firstFile = context.textFile(args(1), 2).cache()
    val secondFile = context.textFile(args(2), 2).cache()

    println("Unique words:")
    val uniqueWords = this.findUniqueWords(firstFile).collect()
    this.showArray(uniqueWords)

    val uniqueWordsCount = this.findUniqueWordsCount(firstFile).collect()
    println("Unique words count: ")
    uniqueWordsCount.foreach(item => println(item._1 + ": " + item._2))

    println("Word sets:")
    val wordSets = this.findWordSets(firstFile, secondFile)
    println("Intersection: ")
    showArray(wordSets._1)

    println("First text words: ")
    showArray(wordSets._2)

    println("Second text words: ")
    showArray(wordSets._3)

    System.exit(0)
  }

  def findUniqueWordsCount(file : RDD[String]): RDD[(String, Int)] = {
    val excluded = Array(',', '.', '(', ')', '{', '}')

    file
      .map(line => line.toLowerCase().filter(c => !excluded.contains(c)))
      .map(line => line.split(' ').groupBy(word => word))
      .flatMap(group => group.keys.map(key => (key, group(key).length)))
      .reduceByKey(_ + _)
  }

  def findUniqueWords(file : RDD[String]): RDD[String] = {
    val delimeter = '|'
    val excluded = Array(',', '.', '(', ')', '{', '}')

    file
      .flatMap(line => line.toLowerCase().filter(c => !excluded.contains(c)).split(' '))
      .distinct()
      .sortBy(word => word)
  }

  def findWordSets(first : RDD[String], second : RDD[String]) : (Array[String], Array[String], Array[String]) = {
    val firstUniqueWords = findUniqueWords(first)
    val secondUniqueWords = findUniqueWords(second)

    val intersection = firstUniqueWords.intersection(secondUniqueWords)
    val firstSubtraction = firstUniqueWords.subtract(secondUniqueWords)
    val secondSubtraction = secondUniqueWords.subtract(firstUniqueWords)

    (intersection.collect(), firstSubtraction.collect(), secondSubtraction.collect())
  }

  def showArray(items : Array[String]): Unit = {
    println()
    items.foreach(item => println(item))
    println()
  }
}